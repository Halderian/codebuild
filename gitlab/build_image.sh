#!/usr/bin/env sh

set -x

if [ ! -f Dockerfile ]
then
  echo "file not found: Dockerfile"
  exit 1
fi

if [ -z "$DOCKERHUB_USERNAME" -o "$DOCKERHUB_USERNAME" = "" ]
then
  echo "variable not set: DOCKERHUB_USERNAME"
  exit 1
fi

if [ -z "$DOCKERHUB_PASSWORD" -o "$DOCKERHUB_PASSWORD" = "" ]
then
  echo "variable not set: DOCKERHUB_PASSWORD"
  exit 1
fi

VERSION=`grep ^FROM Dockerfile | awk -F":" '{print $2}'`

docker build . -t $DOCKERHUB_USERNAME/codebuild:$VERSION && \
  docker login --username $DOCKERHUB_USERNAME --password $DOCKERHUB_PASSWORD && \
  docker push $DOCKERHUB_USERNAME/codebuild:$VERSION && \
  docker tag $DOCKERHUB_USERNAME/codebuild:$VERSION $DOCKERHUB_USERNAME/codebuild:latest && \
  docker push $DOCKERHUB_USERNAME/codebuild:latest
