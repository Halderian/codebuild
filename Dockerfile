FROM alpine:3.9

RUN apk update --no-cache && \
  apk upgrade --no-cache && \
  apk add --no-cache bash

CMD ["/bin/sh"]
